package sentifi.model;

public class Price {

    private String ticker;
    private String date;
    private Double open;
    private Double high;
    private Double low;
    private Double close;
    private Double volume;
    private Double twapOpen;
    private Double twapHigh;
    private Double twapLow;
    private Double twapClose;
    private Double sma;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getTwapOpen() {
        return twapOpen;
    }

    public void setTwapOpen(Double twapOpen) {
        this.twapOpen = twapOpen;
    }

    public Double getTwapHigh() {
        return twapHigh;
    }

    public void setTwapHigh(Double twapHigh) {
        this.twapHigh = twapHigh;
    }

    public Double getTwapLow() {
        return twapLow;
    }

    public void setTwapLow(Double twapLow) {
        this.twapLow = twapLow;
    }

    public Double getTwapClose() {
        return twapClose;
    }

    public void setTwapClose(Double twapClose) {
        this.twapClose = twapClose;
    }

    public Double getSma() {
        return sma;
    }

    public void setSma(Double sma) {
        this.sma = sma;
    }
}
