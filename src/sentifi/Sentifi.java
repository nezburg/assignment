package sentifi;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import sentifi.model.Price;
import sentifi.utils.AppUtil;
import sentifi.utils.Constants;
import sentifi.utils.JSONUtil;

public class Sentifi extends javax.swing.JFrame {

    private Price averagePrice;
    private String symbol;

    public Sentifi() {
        initComponents();
        ((JTextField) dcStartDate.getDateEditor().getUiComponent()).setText("2018-01-01");
        populateTable(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        dcStartDate = new com.toedter.calendar.JDateChooser();
        jLabel27 = new javax.swing.JLabel();
        edtTickerSymbol = new javax.swing.JTextField();
        btnQuery = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jlTicker = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jlDate = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jlOpen = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jlHigh = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jlLow = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jlTwapLow = new javax.swing.JLabel();
        jlTwapHigh = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jlTwapOpen = new javax.swing.JLabel();
        jlVolume = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jlClose = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jlTwapClose = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jlSMA200 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jlSMA50 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        btnSMA = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jlLWMA15 = new javax.swing.JLabel();
        jlLWMA50 = new javax.swing.JLabel();
        btnUpdatefiles = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        getContentPane().setLayout(new java.awt.GridLayout());

        jPanel1.setBackground(new java.awt.Color(153, 153, 255));

        dcStartDate.setAlignmentX(0.0F);
        dcStartDate.setAlignmentY(0.0F);
        dcStartDate.setDateFormatString("yyyy-MM-dd");

        jLabel27.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel27.setText("Ticker Symbol");

        btnQuery.setText("Query");
        btnQuery.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQueryActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel2.setText("Start Date");

        jTable.setBackground(new java.awt.Color(153, 153, 255));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable.setShowGrid(false);
        jScrollPane1.setViewportView(jTable);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnQuery, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dcStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edtTickerSymbol))))
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel27)
                            .addComponent(edtTickerSymbol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)
                        .addComponent(dcStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnQuery)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        jPanel2.setBackground(new java.awt.Color(255, 204, 255));

        jLabel1.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel1.setText("Ticker:");

        jlTicker.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlTicker.setText("---");

        jLabel3.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel3.setText("Date:");

        jlDate.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlDate.setText("---");

        jLabel5.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel5.setText("Open:");

        jlOpen.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlOpen.setText("---");

        jLabel7.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel7.setText("High:");

        jlHigh.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlHigh.setText("---");

        jLabel9.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel9.setText("Low:");

        jlLow.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlLow.setText("---");

        jLabel11.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel11.setText("TWAP-Low:");

        jlTwapLow.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlTwapLow.setText("---");

        jlTwapHigh.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlTwapHigh.setText("---");

        jLabel14.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel14.setText("TWAP-High:");

        jLabel15.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel15.setText("TWAP-Open:");

        jlTwapOpen.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlTwapOpen.setText("---");

        jlVolume.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlVolume.setText("---");

        jLabel18.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel18.setText("Volume:");

        jLabel19.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel19.setText("Close:");

        jlClose.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlClose.setText("---");

        jLabel21.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel21.setText("TWAP-Close:");

        jlTwapClose.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlTwapClose.setText("---");

        jLabel23.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel23.setText("SMA-200:");

        jlSMA200.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlSMA200.setText("---");

        jLabel25.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel25.setText("SMA-50:");

        jlSMA50.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlSMA50.setText("---");

        jSeparator1.setBackground(new java.awt.Color(102, 102, 102));

        jSeparator2.setBackground(new java.awt.Color(102, 102, 102));

        btnSMA.setText("Query SMA");
        btnSMA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSMAActionPerformed(evt);
            }
        });

        jSeparator3.setBackground(new java.awt.Color(102, 102, 102));

        jLabel24.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel24.setText("LWMA-50:");

        jLabel26.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jLabel26.setText("LWMA-15:");

        jlLWMA15.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlLWMA15.setText("---");

        jlLWMA50.setFont(new java.awt.Font("Myanmar MN", 1, 13)); // NOI18N
        jlLWMA50.setText("---");

        btnUpdatefiles.setText("Update Files");
        btnUpdatefiles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatefilesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnUpdatefiles, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addComponent(jLabel11)
                            .addComponent(jLabel14))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlTwapHigh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlTwapLow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlTwapClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addComponent(jLabel25))
                        .addGap(41, 41, 41)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlSMA50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlSMA200, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel18)
                            .addComponent(jLabel19)
                            .addComponent(jLabel9)
                            .addComponent(jLabel7)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlTicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlOpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlHigh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlLow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlVolume, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlTwapOpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSMA))
                    .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(jLabel26))
                        .addGap(41, 41, 41)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlLWMA15, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                            .addComponent(jlLWMA50, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jlTicker))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jlDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jlOpen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jlHigh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jlLow))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jlClose))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jlVolume))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jlTwapOpen))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jlTwapHigh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jlTwapLow))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jlTwapClose))
                .addGap(1, 1, 1)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(jlSMA50))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jlSMA200))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSMA)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(jlLWMA15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jlLWMA50))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(btnUpdatefiles)
                .addGap(22, 22, 22))
        );

        getContentPane().add(jPanel2);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void populateData(Price price) {
        jlTicker.setText(symbol.toUpperCase());

        jlDate.setText(price.getDate());
        jlOpen.setText(price.getOpen().toString());
        jlHigh.setText(price.getHigh().toString());
        jlLow.setText(price.getLow().toString());
        jlClose.setText(price.getClose().toString());
        jlVolume.setText(price.getVolume().toString());

        jlTwapOpen.setText(price.getTwapOpen().toString());
        jlTwapHigh.setText(price.getTwapHigh().toString());
        jlTwapLow.setText(price.getTwapLow().toString());
        jlTwapClose.setText(price.getTwapClose().toString());

        jlSMA50.setText("---");
        jlSMA200.setText("---");
        jlSMA50.setForeground(Color.BLACK);
    }

    private void resetData() {
        jlTicker.setText(symbol.toUpperCase());
        jlDate.setText("---");
        jlOpen.setText("---");
        jlHigh.setText("---");
        jlLow.setText("---");
        jlClose.setText("---");
        jlVolume.setText("---");
        jlTwapOpen.setText("---");
        jlTwapHigh.setText("---");
        jlTwapLow.setText("---");
        jlTwapClose.setText("---");
        jlSMA50.setText("---");
        jlSMA200.setText("---");
        jlSMA50.setForeground(Color.BLACK);
    }

    private void populateTable(ArrayList<Price> prices) {
        String[] columns = new String[]{"Date", "Open", "High", "Low", "Close", "Volume"};
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        if (prices != null && !prices.isEmpty()) {
            for (Price price : prices) {
                Object[] objs = {
                    price.getDate(), price.getOpen(), price.getHigh(),
                    price.getLow(), price.getClose(), price.getVolume()
                };
                tableModel.addRow(objs);
            }
        } else {
            int rowCount = tableModel.getRowCount();
            for (int i = rowCount - 1; i >= 0; i--) {
                tableModel.removeRow(i);
            }
        }
        jTable.setModel(tableModel);
        jTable.setFillsViewportHeight(true);
    }

    private void setLWMAPrediction(ArrayList<Price> prices) {
        String lwma15 = AppUtil.calcLWMAPrediction15Days(prices);
        String lwma50 = AppUtil.calcLWMAPrediction50Days(prices);
        jlLWMA15.setText(lwma15);
        jlLWMA50.setText(lwma50);
    }

    private void btnQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQueryActionPerformed
        symbol = edtTickerSymbol.getText();
        String startDate = ((JTextField) dcStartDate.getDateEditor().getUiComponent()).getText();
        System.out.println(startDate);
        if (symbol.isEmpty()) {
            return;
        }
        populateTable(null);
        resetData();
        try {
            JSONUtil utils = new JSONUtil();
            JSONObject res = utils.getJSONFromUrl(Constants.getQueryUrl(symbol, startDate));
            if (res != null) {
                JSONObject content = (JSONObject) res.get("dataset");
                System.out.println("Dataset =>" + content.toJSONString());
                JSONArray data = (JSONArray) content.get("data");
                ArrayList<Price> arrPrice = new ArrayList<>();
                for (int i = 0; i < data.size(); i++) {
                    JSONArray priceData = (JSONArray) data.get(i);
                    Price price = JSONUtil.priceFromJson(priceData);
                    arrPrice.add(price);
                }
                averagePrice = AppUtil.calcAndSetAverage(arrPrice, false);
                averagePrice.setTicker(symbol.toUpperCase());
                populateData(averagePrice);
                populateTable(arrPrice);
                setLWMAPrediction(arrPrice);
            }
        } catch (IOException | ParseException | JSONException ex) {
            Logger.getLogger(Sentifi.class.getName()).log(Level.SEVERE, null, ex);
            AppUtil.showErrorMsg(this, "Error! check inputted ticker symbol or internet connection");
        }
    }//GEN-LAST:event_btnQueryActionPerformed

    private void btnSMAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSMAActionPerformed
        String startDate = ((JTextField) dcStartDate.getDateEditor().getUiComponent()).getText();
        if (startDate.isEmpty()) {
            return;
        }
        String sma50Date, sma200Date;
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        c.add(Calendar.DATE, -50);
        sma50Date = AppUtil.getFormattedDate(c.getTime());
        c.setTime(new Date());
        c.add(Calendar.DATE, -200);
        sma200Date = AppUtil.getFormattedDate(c.getTime());
        System.out.println(sma50Date + " <-> " + sma200Date);

        try {
            JSONUtil utils = new JSONUtil();
            JSONObject res = utils.getJSONFromUrl(Constants.getQueryUrl(symbol, sma200Date));
            if (res != null) {
                JSONObject content = (JSONObject) res.get("dataset");
                System.out.println("Dataset =>" + content.toJSONString());
                JSONArray data = (JSONArray) content.get("data");

                ArrayList<Price> arrPrice50 = new ArrayList<>();
                boolean flag = true;
                ArrayList<Price> arrPrice200 = new ArrayList<>();
                for (int i = 0; i < data.size(); i++) {
                    JSONArray priceData = (JSONArray) data.get(i);
                    Price price = JSONUtil.priceFromJson(priceData);
                    arrPrice200.add(price);
                    if (flag && !sma50Date.equals(price.getDate().trim())) {
                        arrPrice50.add(price);
                        flag = false;
                    }
                }

                Price price200 = AppUtil.calcAndSetAverage(arrPrice200, true);
                Price price50 = AppUtil.calcAndSetAverage(arrPrice50, true);

                jlSMA50.setText(price50.getSma().toString());
                jlSMA200.setText(price200.getSma().toString());

                checkProgressForAlert(arrPrice200.get(0), price50, price200);
            }
        } catch (IOException | ParseException | JSONException ex) {
            Logger.getLogger(Sentifi.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSMAActionPerformed

    private void btnUpdatefilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatefilesActionPerformed
        JSONObject object = JSONUtil.priceToJSONObject(averagePrice);
        AppUtil.appendJSONFile(object);
        try {
            AppUtil.writeCSV(averagePrice);
        } catch (IOException ex) {
            Logger.getLogger(Sentifi.class.getName()).log(Level.SEVERE, null, ex);
        }
        AppUtil.showAlertMsg(this, "CSV and JSON the file are updated respectively");
    }//GEN-LAST:event_btnUpdatefilesActionPerformed

    private void checkProgressForAlert(Price priceToday, Price price50, Price price200) {
        jlSMA50.setForeground(Color.BLACK);
        if (price50.getSma() < price200.getSma()) {
            //show alert red color
            double diff = price200.getSma() - price50.getSma();
            jlSMA50.setForeground(Color.RED);
            jlSMA50.setText(jlSMA50.getText());
            String msg = "SMA-50 dropped by (-" + diff + ") in compare to SMA-200";
            AppUtil.showAlertMsg(this, msg);
            AppUtil.appendDATFile(priceToday.getDate() + ":" + msg + ".\n");
        }
        if (price50.getSma() > price200.getSma()) {
            double tenPercentOfAverageVolumeOfLast50Days = (double) (price50.getVolume() * (10.0d / 100.0d));
            if (priceToday.getVolume() > (price50.getVolume() + tenPercentOfAverageVolumeOfLast50Days)) {
                //show alert green color for sales doing great
                jlSMA50.setForeground(Color.GREEN);
                jlSMA50.setText(jlSMA50.getText());
                String msg = "SMA-50 increased as well as the current Volume in compare to SMA-200";
                AppUtil.showAlertMsg(this, msg);
                AppUtil.appendDATFile(priceToday.getDate() + ":" + msg + ".\n");
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Sentifi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Sentifi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Sentifi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Sentifi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Sentifi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnQuery;
    private javax.swing.JButton btnSMA;
    private javax.swing.JButton btnUpdatefiles;
    private com.toedter.calendar.JDateChooser dcStartDate;
    private javax.swing.JTextField edtTickerSymbol;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTable jTable;
    private javax.swing.JLabel jlClose;
    private javax.swing.JLabel jlDate;
    private javax.swing.JLabel jlHigh;
    private javax.swing.JLabel jlLWMA15;
    private javax.swing.JLabel jlLWMA50;
    private javax.swing.JLabel jlLow;
    private javax.swing.JLabel jlOpen;
    private javax.swing.JLabel jlSMA200;
    private javax.swing.JLabel jlSMA50;
    private javax.swing.JLabel jlTicker;
    private javax.swing.JLabel jlTwapClose;
    private javax.swing.JLabel jlTwapHigh;
    private javax.swing.JLabel jlTwapLow;
    private javax.swing.JLabel jlTwapOpen;
    private javax.swing.JLabel jlVolume;
    // End of variables declaration//GEN-END:variables
}
