package sentifi.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sentifi.model.Price;

public class JSONUtil {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    public JSONUtil() {

    }

    public static Price priceFromJson(JSONArray array) {
        Price price = new Price();
        price.setDate(String.valueOf(array.get(Constants.DATE)));
        price.setOpen(Double.valueOf(array.get(Constants.OPEN).toString()));
        price.setHigh(Double.valueOf(array.get(Constants.HIGH).toString()));
        price.setLow(Double.valueOf(array.get(Constants.LOW).toString()));
        price.setClose(Double.valueOf(array.get(Constants.CLOSE).toString()));
        price.setVolume(Double.valueOf(array.get(Constants.VOLUME).toString()));
        return price;
    }

    public static JSONObject priceToJSONObject(Price averagePrice) {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonPrice = new JSONObject();
        jsonPrice.put("Ticker", averagePrice.getTicker());
        jsonPrice.put("Date", averagePrice.getDate());
        jsonPrice.put("Open", averagePrice.getOpen().toString());
        jsonPrice.put("High", averagePrice.getHigh().toString());
        jsonPrice.put("Low", averagePrice.getLow().toString());
        jsonPrice.put("Close", averagePrice.getClose().toString());
        jsonPrice.put("Volume", averagePrice.getVolume().toString());
        jsonPrice.put("TWAP-Open", averagePrice.getTwapOpen().toString());
        jsonPrice.put("TWAP-High", averagePrice.getTwapHigh().toString());
        jsonPrice.put("TWAP-Low", averagePrice.getTwapLow().toString());
        jsonPrice.put("TWAP-Close", averagePrice.getTwapClose().toString());
        jsonObject.put("Price", jsonPrice);
        return jsonObject;
    }

    public JSONObject getJSONFromUrl(String url) throws IOException, org.json.simple.parser.ParseException, JSONException {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        URL oracle = new URL(url);
        URLConnection yc = oracle.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            jsonObject = (JSONObject) parser.parse(inputLine);
            System.out.println(jsonObject.toJSONString());
        }
        in.close();
        return jsonObject;
    }
}
