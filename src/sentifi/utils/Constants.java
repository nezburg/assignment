package sentifi.utils;

public class Constants {

    public static int DATE = 0;
    public static int OPEN = 1;
    public static int HIGH = 2;
    public static int LOW = 3;
    public static int CLOSE = 4;
    public static int VOLUME = 5;

    private static String BaseUrl = "https://www.quandl.com/api/v3/datasets/WIKI/";

    public static String getQueryUrl(String tickerSymbol, String startDate) {
        String apiKey = "&api_key=JGEBLJ5DbExpeLCsXMix";
        return BaseUrl + tickerSymbol + (startDate.isEmpty() ? ".json"+apiKey : ".json?start_date=" + startDate+apiKey);
    }
}
