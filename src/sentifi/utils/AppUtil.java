package sentifi.utils;

import java.awt.Component;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import sentifi.model.Price;

public class AppUtil {

    public static Price calcAndSetAverage(ArrayList<Price> data, boolean isSMA) {
        Price res = new Price();
        int size = data.size();

        double open = 0;
        open = data.stream().map((each) -> each.getOpen()).reduce(open, (accumulator, _item) -> accumulator + _item);
        res.setTwapOpen(averageVal(size, open));

        double high = 0;
        high = data.stream().map((each) -> each.getHigh()).reduce(high, (accumulator, _item) -> accumulator + _item);
        res.setTwapHigh(averageVal(size, high));

        double low = 0;
        low = data.stream().map((each) -> each.getLow()).reduce(low, (accumulator, _item) -> accumulator + _item);
        res.setTwapLow(averageVal(size, low));

        double close = 0;
        close = data.stream().map((each) -> each.getClose()).reduce(close, (accumulator, _item) -> accumulator + _item);
        res.setTwapClose(averageVal(size, close));

        if (!data.isEmpty()) {
            int today = 0;
            res.setDate(data.get(today).getDate());
            res.setOpen(data.get(today).getOpen());
            res.setHigh(data.get(today).getHigh());
            res.setLow(data.get(today).getLow());
            res.setClose(data.get(today).getClose());
            res.setVolume(data.get(today).getVolume());
        }

        if (isSMA) {
            res.setSma(averageVal(size, close));
            double volume = 0;
            volume = data.stream().map((each) -> each.getVolume()).reduce(volume, (accumulator, _item) -> accumulator + _item);
            res.setTwapClose(averageVal(size, volume));
        }

        return res;
    }
    
    public static String calcLWMAPrediction50Days(ArrayList<Price> prices) {
        String res;
        if (!prices.isEmpty() && prices.size() >= 49) {
            double sumOfMultiplier = 1275; //50*51 div by 2 == 50+49+48...+2+1
            double sum = 0;
            int multiplier = 50;
            for (int i = 0; i < 50; i++) {
                double closeVal = prices.get(i).getClose();
                sum += multiplier * closeVal;
                multiplier--;
            }
            double resultLWMA50 = sum / sumOfMultiplier;
            res = AppUtil.round(resultLWMA50) + "";
        } else {
            res = "Prediction requires 50 minimum records";
        }
        return res;
    }
    
    public static String calcLWMAPrediction15Days(ArrayList<Price> prices) {
        String res;
        if (!prices.isEmpty() && prices.size() >= 14) {
            double sumOfMultiplier = 120;
            double sum = 0;
            int multiplier = 15;
            //Prices date was ordered DESC
            for (int i = 0; i < 15; i++) {
                double closeVal = prices.get(i).getClose();
                sum += multiplier * closeVal;
                multiplier--;
            }
            double resultLWMA15 = sum / sumOfMultiplier;
            res = AppUtil.round(resultLWMA15) + "";
        } else {
            res = "Prediction requires 15 minimum records";
        }
        return res;
    }

    public static Double round(double value) {
        return (double) Math.round(value * 100) / 100;
    }

    private static Double averageVal(int size, double sumValue) {
        double average = (Double) sumValue / size;
        return (double) Math.round(average * 100) / 100;
    }

    public static String getFormattedDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    public static void appendDATFile(String msg) {
        try (FileWriter file = new FileWriter("alert.dat", true)) {
            file.write(msg);
            file.flush();
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void appendJSONFile(JSONObject object) {
        try (FileWriter file = new FileWriter("prices.json", true)) {
            file.write(object.toJSONString());
            file.flush();
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void writeCSV(Price price) throws IOException {
        String csvFile = "prices.csv";
        FileWriter writer = new FileWriter(csvFile, true);
        writeLine(writer, price, ',');
        writer.flush();
        writer.close();
    }

    private static void writeLine(Writer w, Price price, char separators) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(price.getTicker());
        sb.append(separators);
        sb.append(price.getDate());
        sb.append(separators);
        sb.append(price.getOpen().toString());
        sb.append(separators);
        sb.append(price.getHigh().toString());
        sb.append(separators);
        sb.append(price.getLow().toString());
        sb.append(separators);
        sb.append(price.getClose().toString());
        sb.append(separators);
        sb.append(price.getVolume().toString());
        sb.append(separators);
        sb.append(price.getTwapOpen().toString());
        sb.append(separators);
        sb.append(price.getTwapHigh().toString());
        sb.append(separators);
        sb.append(price.getTwapLow().toString());
        sb.append(separators);
        sb.append(price.getTwapClose().toString());
        sb.append(".\n");
        w.append(sb.toString());
    }
    
    
    public static void showAlertMsg(Component component, String msg) {
        JOptionPane.showMessageDialog(component, msg);
    }

    public static void showErrorMsg(Component component, String msg) {
        JOptionPane.showMessageDialog(component, msg, "Error!", JOptionPane.ERROR_MESSAGE);
    }
}
