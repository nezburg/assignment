# Assignment
 
 The IDE used is NetBeans, Java 8 as the requirement
 
## Prerequisites
 
 You'll need to have Java installed you as to execute the .jar file.
 
## Execution
 
 Where to find the .jar file to be executed, it's self explanatory using the common interaction design in the developmental proccess
 
 ```
 cd dist/Sentifi.jar
 ```
 
 The following ticker symbol can be used to query data on the app, build with registered account, therefore no worries about limited requests that can be made, have fun 
 
 | No      | #1  | #2  | #3  | #4  | #5  | #6  | #7  | #8  | #9  | #10 |
 | :----:  | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
 | Ticker  | GE  | ACN | ACE | AIG | AON | BWA | CAT | CVC | DHI | DNB |
 
 
## Running the tests
 
 Testing was made majorly for the operations which are present in the AppUtils file because there's the apps logic are being computed. See AppUtils and it's test file for more info
 
 
### Author
 
 Donald
