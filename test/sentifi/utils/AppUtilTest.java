package sentifi.utils;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import sentifi.model.Price;

public class AppUtilTest {
    private static final double DELTA = 1e-15;
    
    public AppUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcAndSetAverage method, of class AppUtil.
     */
    @Test
    public void testCalcAndSetAverage() {
        System.out.println("calcAndSetAverage");
        ArrayList<Price> data = new ArrayList<>();
        //now setting all prices value incremeneting from 1 to 10 for ten items
        for (double i = 1; i < 11; i++) {
            Price testPrice = new Price();
            testPrice.setOpen(i);
            testPrice.setHigh(i);
            testPrice.setLow(i);
            testPrice.setClose(i);
            testPrice.setVolume(i);
            testPrice.setDate("");
            data.add(testPrice);
        }
        boolean isSMA = false;
        double expResult = 5.5; // (10+9+8+7..+2+1)/10 or (10*11/2)/10 = 55/10 = 5.5
        //TWAP-OPEN
        double resultTWAPOpen = AppUtil.calcAndSetAverage(data, isSMA).getTwapOpen();
        assertEquals(expResult, resultTWAPOpen, DELTA);
        //TWAP-HIGH
        double resultTWAPHigh = AppUtil.calcAndSetAverage(data, isSMA).getTwapHigh();
        assertEquals(expResult, resultTWAPHigh, DELTA);
        //TWAP-LOW
        double resultTwapLow = AppUtil.calcAndSetAverage(data, isSMA).getTwapLow();
        assertEquals(expResult, resultTwapLow, DELTA);
        //TWAP-CLOSE, Volume e.t.c.
        
        double resultSMA = AppUtil.calcAndSetAverage(data, true).getSma();
        assertEquals(expResult, resultSMA, DELTA);
    }

    /**
     * Test of calcLWMAPrediction50Days method, of class AppUtil.
     */
    @Test
    public void testCalcLWMAPrediction50Days() {
        System.out.println("calcLWMAPrediction50Days");
        ArrayList<Price> prices = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            Price price = new Price();
            price.setClose(5.0);
            prices.add(price);
        }
        String expResult = "5.0";
        String result = AppUtil.calcLWMAPrediction50Days(prices);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcLWMAPrediction15Days method, of class AppUtil.
     */
    @Test
    public void testCalcLWMAPrediction15Days() {
        System.out.println("calcLWMAPrediction15Days");
        ArrayList<Price> prices = new ArrayList<>();
        for (double i = 0; i < 15; i++) {
            Price price = new Price();
            price.setClose(2d);
            prices.add(price);
        }
        String expResult = "2.0";
        String result = AppUtil.calcLWMAPrediction15Days(prices);
        assertEquals(expResult, result);
    }

    /**
     * Test of round method, of class AppUtil.
     */
    @Test
    public void testRound() {
        System.out.println("round");
        double value = 12.5260623;
        Double expResult = 12.53;//Expected to returns max of two decimal place
        Double result = AppUtil.round(value);
        assertEquals(expResult, result);
    }
}
